for $h in distinct-values((doc("voc.xml")/VOC/voyage/leftpage/harbour, doc("voc.xml")/VOC/voyage/leftpage/destination/harbour))
order by $h 
let $departures := count(
                        for $a in doc("voc.xml")/VOC/voyage
                        where $a/leftpage/harbour = $h
                        return $a 
                      )
let $destinations := count(
                        for $b in doc("voc.xml")/VOC/voyage
                        where $b/leftpage/destination/harbour = $h
                        return $b
                        )


return <harbour name="{$h}">
          <nr-of-depatures>{$departures}</nr-of-depatures>
          <nr-of-destinations>{$destinations}</nr-of-destinations>
       </harbour>
       
