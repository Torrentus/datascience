let $ms := 

for $v in doc("voc.xml")/VOC/voyage
where not(exists($v/leftpage/boatname))
return $v

for $v in subsequence($ms,1,1)
return $v
