select
    XMLELEMENT ( NAME "name", name) as "<name>",
    XMLELEMENT ( NAME "year", year) as "<year>",
    XMLELEMENT ( NAME "rating", rating) as "<rating>"
from movie;
