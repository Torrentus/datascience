select
    XMLCONCAT (
        XMLELEMENT ( NAME "name", name),
        XMLELEMENT ( NAME "year", year),
        XMLELEMENT ( NAME "rating", rating)
    ) as "result"
from movie;
