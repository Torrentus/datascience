select
    XMLELEMENT( NAME "movie", xmlattributes(m.mid as "id")),
        XMLCONCAT (
            XMLELEMENT ( NAME "name", m.name),
            XMLELEMENT ( NAME "year", m.year),
            XMLELEMENT ( NAME "rating", m.rating),
            (select
                XMLAGG (
                    XMLELEMENT ( NAME "role",  a.role )
                )
             from acts a where a.mid = m.mid),
             (select
                XMLAGG (
                    XMLELEMENT ( NAME "director",  (select name from person p where p.pid = d.pid))
                )
             from directs d where d.mid = m.mid)
        )
    ) as "result"
from movie m;
