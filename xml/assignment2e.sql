select
    XMLELEMENT( NAME "movie", xmlattributes(m.mid as "id")),
        XMLCONCAT (
            XMLELEMENT ( NAME "name", m.name),
            XMLELEMENT ( NAME "year", m.year),
            XMLELEMENT ( NAME "rating", m.rating),
            (select
                XMLAGG (
                    XMLELEMENT ( NAME "role",  a.role )
                )
             from acts a where a.mid = m.mid)
        )
    ) as "result"
from movie m;
