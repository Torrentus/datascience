declare function local:Years(){
    for $d in distinct-values((doc("eprints-2015-02-01.xml")/eprints/paper/date_issue))
      let $y := fn:substring($d, 1, 4)
  return $y
};

declare function local:getAuthorsPapers($y){
  for $p in doc("eprints-2015-02-01.xml")/eprints/paper
  where fn:substring($p/date_issue, 1,4) = $y
  return $p/author
};

declare function local:getAuthors($y){
  let $ap := local:getAuthorsPapers($y)
  for $a in distinct-values($ap)
  let $nrOfPapers := count(for $p in $ap where $a = $p return $p)   
  order by $nrOfPapers descending
  return <author name="{$a}" nrOfPapers="{$nrOfPapers}"/>
};

for $y in distinct-values(local:Years())
  order by $y descending
  return <year year="{$y}">
            {local:getAuthors($y)[1]}
         </year>
  