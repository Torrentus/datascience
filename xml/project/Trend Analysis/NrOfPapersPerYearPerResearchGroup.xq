declare function local:Years(){
    for $d in distinct-values((doc("eprints-2015-02-01.xml")/eprints/paper/date_issue))
      let $y := fn:substring($d, 1, 4)
     
  return $y
};

declare function local:getResearchgroupPapers($y){
  for $p in doc("eprints-2015-02-01.xml")/eprints/paper
  where fn:substring($p/date_issue, 1,4) = $y
  return $p/first_group 
};

declare function local:getResearchgroup($y){
  let $rp := local:getResearchgroupPapers($y)
  for $v in distinct-values($rp)
  let $nrOfPapers := count(for $p in $rp where $v = $p return $p)  
  return <vakgroep name="{$v}" amountOfPapers="{$nrOfPapers}"/>
};

for $y in distinct-values(local:Years())
  order by $y ascending
  return <year year="{$y}">
            {local:getResearchgroup($y)}
         </year>
  