declare function local:Years(){
    for $d in distinct-values((doc("eprints-2015-02-01.xml")/eprints/paper/date_issue))
      let $y := fn:substring($d, 1, 4)
     
  return $y
};

declare function local:getAmountofPapers($y){
  let $a :=count(
  for $p in doc("eprints-2015-02-01.xml")/eprints/paper
  where fn:substring($p/date_issue, 1, 4) = $y
  return $p
  )
  return $a
};


for $y in distinct-values(local:Years())
  order by $y ascending
  return <detail year="{$y}" nrOfPapers="{local:getAmountofPapers($y)}"/>
  