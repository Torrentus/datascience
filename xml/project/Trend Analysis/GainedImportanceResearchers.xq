declare function local:Years(){
    for $d in distinct-values((doc("eprints-2015-02-01.xml")/eprints/paper/date_issue))
      let $y := fn:substring($d, 1, 4)
  
  order by $d descending   
  return $y
};


declare function local:getAuthorsPapers($y1, $y2){
  for $p in doc("eprints-2015-02-01.xml")/eprints/paper
  let $t := fn:substring($p/date_issue, 1, 4)
  where $t = $y1 or $t = $y2
  for $a in $p/author
  return <paper author="{$a}" year="{$t}"/>
};

declare function local:somefunction($y1, $y2){
  let $ap := local:getAuthorsPapers($y1, $y2)
  
  let $totalNrOfPapersY1 := count(for $p in $ap where $p/@year = $y1 return $p)
  let $totalNrOfPapersY2 := count(for $p in $ap where $p/@year = $y2 return $p)
  
  for $a in distinct-values($ap/@author)
  let $nrOfPapersY1 := count(for $p in $ap where $p/@year = $y1 and $p/@author = $a return $p)
  let $nrOfPapersY2 := count(for $p in $ap where $p/@year = $y2 and $p/@author = $a return $p)
  let $ratioY1 := $nrOfPapersY1 div $totalNrOfPapersY1
  let $ratioY2 := $nrOfPapersY2 div $totalNrOfPapersY2
  let $gained := if($ratioY1 = 0) then $ratioY2 else ($ratioY2 - $ratioY1) div $ratioY1 
  order by $gained descending
  return <author name="{$a}" ratio="{$gained}"/>
  
};

local:somefunction('2006', '2014')
