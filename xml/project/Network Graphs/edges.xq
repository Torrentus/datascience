declare function local:XMLprefix(){
'<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns 
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
<key attr.name="Vakgroep" attr.type="string" for="edge" id="Vakgroep"/>
<graph id="E" edgedefault="undirected">'
};

declare function local:edgelist(){
  for $p in doc("eprints-2015-02-01.xml")/eprints/paper
  let $fa := ($p/author)[1]
  for $t in distinct-values($p/author)
  where $t !=  $fa
  return <edge source="{$fa}" target="{$t}"><data key="Vakgroep">{$p/first_group/text()}</data></edge>
};

declare function local:nodelist(){
  for $a in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/author)  
  return <node id="{$a}"/>
};


local:XMLprefix(), local:edgelist(), local:nodelist(), "</graph></graphml>"
