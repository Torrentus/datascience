for $a in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/author)
let $pages := sum(
  for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
  where $paper/author = $a
  return $paper/pages
)
order by $pages descending
return <author name="{$a}" nrpages="{$pages}"/>
