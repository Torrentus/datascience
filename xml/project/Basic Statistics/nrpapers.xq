for $a in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/author)
let $count := count(
  for $c in doc("eprints-2015-02-01.xml")/eprints/paper/author
  where $c = $a
  return $c
)
order by $count descending
return <author name="{$a}" nrofpapers="{$count}"/>
