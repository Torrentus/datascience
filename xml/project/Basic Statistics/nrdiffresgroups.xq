for $a in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/author)
let $count := count(distinct-values(
  for $c in doc("eprints-2015-02-01.xml")/eprints/paper
  where $c/author = $a
  return $c/first_group
))
order by $count descending
return <author name="{$a}" nrofdistinctgroups="{$count}"/>
