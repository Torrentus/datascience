for $a in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/author[1])
let $count := count(
  for $c in doc("eprints-2015-02-01.xml")/eprints/paper/author[1]
  where $c = $a
  return $c
)
order by $count descending
return <author name="{$a}" nroffirstpapers="{$count}"/>
