for $m in distinct-values(doc("voc.xml")/VOC/voyage/leftpage/master)
let $voyages := count(
  for $v in doc("voc.xml")/VOC/voyage
  where $v/leftpage/master = $m
  return $v
)

return <master name="{$m}" nrofvoyages="{$voyages}"/>
