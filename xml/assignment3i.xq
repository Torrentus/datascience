let $y :=
  for $v in doc("voc.xml")/VOC/voyage
  where $v/leftpage/master = "Jakob de Vries"
  order by xs:date($v/leftpage/departure)
  return $v

for $v in subsequence($y,1,1)
return $v