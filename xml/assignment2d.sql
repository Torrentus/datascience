select
    XMLELEMENT( NAME "movie", xmlattributes(mid as "id"),
        XMLCONCAT (
            XMLELEMENT ( NAME "name", name),
            XMLELEMENT ( NAME "year", year),
            XMLELEMENT ( NAME "rating", rating)
        )
    ) as "result"
from movie;
