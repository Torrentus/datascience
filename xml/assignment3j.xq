let $v := count(distinct-values(doc("voc.xml")/VOC/voyage))
let $b := count(distinct-values(doc("voc.xml")/VOC/voyage/leftpage/boatname))
let $m := count(distinct-values(doc("voc.xml")/VOC/voyage/leftpage/master))

return <totals>
<voyages> {$v} </voyages>
<boats> {$b} </boats>
<masters> {$m}</masters>
</totals>